FROM debian:buster-slim

# Отключает интерактивный режим (меню) при установке пакетов
ENV DEBIAN_FRONTEND=noninteractive 
# Установка в «true» вызовет установку флага seen для вопросов заданных неинтерактивным интерфейсом.
ENV DEBCONF_NONINTERACTIVE_SEEN=true

ENV TZ=Europe/Moscow

RUN apt update -y
RUN apt install -y git python3 python3-pip

COPY ./app /app

WORKDIR app

RUN pip3 install Flask
RUN pip3 install gunicorn

EXPOSE 8000
#CMD ["python3", "app.py"]
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "--log-level", "debug", "app:app"]
