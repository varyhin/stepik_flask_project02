from flask import Flask
from flask import render_template
from flask import request
from data import goals, teachers
from datetime import datetime
import json
import uuid
import random
import os.path

# https://emojipedia.org 
emoji_dict = {'travel': '⛱️', 'study': '📘', 'work': '👔', 'relocate': '📦'}

week_days_ru_dict = {'mon': 'Понедельник', 'tue': 'Вторник', 'wed': 'Среда', 'thu': 'Четверг', 'fri': 'Пятница', 'sat': 'Суббота', 'sun': 'Воскресенье'}

teachers_json = json.dumps(teachers) # По заданию. Зачем не понял.

# now_week_day = datetime.today().strftime('%a') # mon, tue, wed ..... 
# now_hour = datetime.today().time().replace(minute=00).strftime('%H:%M') # 15:00

def save_to_json_file(filename, data):
    if os.path.exists(filename):
        with open(filename, 'r') as f:
            file_content = f.read() # Строка
            file_content = json.loads(file_content) # Список
        file_content.update(data)
        file_content = json.dumps(file_content)
    else:
        file_content = json.dumps(data)
        
    with open(filename, 'w') as f:
        f.write(file_content)

app = Flask(__name__)

@app.route('/')
def render_main():
    return render_template('index.html',
    goals = goals,
    teachers = random.sample(teachers, 6),
    emoji_dict = emoji_dict,
    week_days_ru_dict = week_days_ru_dict,

    )

@app.route('/goals/<goal_name>/')
def render_goals(goal_name):
    return render_template('goal.html',
    goal_name = goal_name,
    goals = goals,
    teachers = teachers,
    emoji_dict = emoji_dict
    )
    
@app.route('/profiles/<profile_id>/')
def render_profiles(profile_id):
    return render_template('profile.html',
    profile_id = profile_id,
    goals = goals,
    teachers = teachers,
    week_days_ru_dict = week_days_ru_dict
    )

@app.route('/booking/<profile_id>/<booking_day>/<booking_time>/')
def render_booking(profile_id, booking_day, booking_time):
    return render_template('booking.html',
    profile_id = profile_id,
    booking_day = booking_day,
    booking_time = booking_time,
    teachers = teachers,
    week_days_ru_dict = week_days_ru_dict
    )
    
@app.route('/booking_done/', methods=['POST'])
def render_booking_done():
    clientWeekday = request.form.get('clientWeekday')
    clientTime = request.form.get('clientTime')
    clientTeacher = request.form.get('clientTeacher')
    clientName = request.form.get('clientName')
    clientPhone = request.form.get('clientPhone')

    # сохраните данные в JSON файле booking.json

    booking_uuid = str(uuid.uuid4())
    booking_at = datetime.today().strftime('%Y.%m.%d %H:%M:%S')
    
    data = {booking_uuid: 
            {'clientTeacher': clientTeacher, 
            'clientWeekday': clientWeekday, 
            'clientTime': clientTime, 
            'clientName': clientName, 
            'clientPhone': clientPhone, 
            'booking_at': booking_at}}
    
    save_to_json_file('booking.json', data)

    return render_template('booking_done.html', 
    booking_uuid = booking_uuid,
    booking_at = booking_at,
    clientWeekday = clientWeekday,
    clientTime = clientTime,
    clientName = clientName,
    clientPhone = clientPhone,
    week_days_ru_dict = week_days_ru_dict
    )

@app.route('/request/')
def render_request():
    return render_template('request.html')

def render_booking(profile_id, booking_day, booking_time):
    return render_template('booking.html',
    profile_id = profile_id,
    booking_day = booking_day,
    booking_time = booking_time,
    teachers = teachers,
    week_days_ru_dict = week_days_ru_dict
    )

@app.route('/request_done/', methods=['POST'])
def render_request_done():
    request_goal = request.form.get('request_goal')
    request_time = request.form.get('request_time')
    request_client_name = request.form.get('request_client_name')
    request_client_phone = request.form.get('request_client_phone')

    # сохраните данные в JSON файле request.json

    request_uuid = str(uuid.uuid4())
    request_at = datetime.today().strftime('%Y.%m.%d %H:%M:%S')
    
    data = {request_uuid: 
            {'request_goal': request_goal, 
            'request_time': request_time, 
            'request_client_name': request_client_name, 
            'request_client_phone': request_client_phone, 
            'request_at': request_at}}
    
    save_to_json_file('request.json', data)

    return render_template('request_done.html',
    request_uuid = request_uuid,
    request_at = request_at,
    request_goal = request_goal,
    request_time = request_time,
    request_client_name = request_client_name,
    request_client_phone = request_client_phone,
    goals = goals
    )

#app.run('0.0.0.0', 8000, debug=True)
if __name__ == '__main__':
    app.run()
